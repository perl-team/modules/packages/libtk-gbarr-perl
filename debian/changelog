libtk-gbarr-perl (2.08-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Carlo Segre from Uploaders. Thanks for your work!
  * Remove Rene Mayorga from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

  [ gregor herrmann ]
  * debian/rules: make usage of $Config{vendorarch}
    cross-build-compatible.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sat, 06 Nov 2021 02:18:07 +0100

libtk-gbarr-perl (2.08-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Wed, 06 Jan 2021 16:38:09 +0100

libtk-gbarr-perl (2.08-2) unstable; urgency=low

  [ Rene Mayorga ]
  * Email change: Rene Mayorga -> rmayorga@debian.org

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Damyan Ivanov ]
  * add -a option to xvfb-run invocation

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Remove Gustavo Franco from Uploaders (closes: #729420)

  [ gregor herrmann ]
  * Switch to "3.0 (quilt)" source format.
  * Modernize debian/rules. And use $Config{vendorarch} as a preparation
    for the multi-arched perl 5.20. (Closes: #753049)
  * Fix example script hashbang in debian/rules instead of patch.
  * debian/copyright: refresh license stanzas.
  * debian/copyright: update list of copyright holders for debian/*.
  * fix_pod_errors.patch: add DEP3 headers and forward.
  * Add new patch to fix a spelling mistake.
  * Bump debhelper compatibility level to 8.
  * Lowercase short description.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 28 Jun 2014 21:32:31 +0200

libtk-gbarr-perl (2.08-1) unstable; urgency=low

  [ Rene Mayorga ]
  * New upstream release
  * debian/control
    + set debhelper version to 7
    + bump standards-version to 3.8.0
    + Add ${misc:Depends} to Depends
    + add myself to uploaders
    + remove xbase-clients B-D-I and add xvfb, xauth and
      xfonts-base
    + add quilt to B-D
  * debian/rules
    + Refresh with dh-make-perl -R --dh7
    + Add workaround to run test with xvfp-run
    + Add workaround to override perms for examples and
      remove /demos and move usr/lib/perl5/Tk to usr/share/perl5/Tk
  * Add fix_pod_errors.patch to fix POD errors.
  * debian/copyright + use (new)format
  * Add shebang_examples.patch to use /usr/bin/perl
    instead of /usr/local/bin/perl

  [ gregor herrmann ]
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * debian/copyright: further formal improvements.
  * debian/control: change my email address.

 -- Rene Mayorga <rmayorga@debian.org.sv>  Tue, 07 Oct 2008 16:55:37 -0600

libtk-gbarr-perl (2.07-4) unstable; urgency=medium

  * create usr/share/perl5 before moving Tk there from usr/lib/perl5
    Closes: #479918 -- FTBFS. Thanks, Lucas!
  * add myself to Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Wed, 07 May 2008 13:57:31 +0300

libtk-gbarr-perl (2.07-3) unstable; urgency=low

  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/rules:
    - delete /usr/lib/perl5 only if it exists (closes: #467925)
    - update with dh-make-perl's help
    - install examples/* to usr/share/doc/libtk-gbarr-perl/examples instead
      of /usr/share/perl5/Tk/demos
  * debian/watch: use dist-based URL.
  * Set Standards-Version to 3.7.3 (no changes).
  * debian/copyright: use author-agnostic download URL.
  * Enable tests in debian/rules and add xvfb, xbase-clients, xfonts-base to
    build dependencies.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Wed, 05 Mar 2008 20:27:22 +0100

libtk-gbarr-perl (2.07-2) unstable; urgency=low

  * Add dh_md5sums to debian/rules.
  * Don't ignore errors of make clean.
  * Move to debhelper 5
  * Remove unneeded commented dh_* calls

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri, 17 Aug 2007 13:31:27 +0200

libtk-gbarr-perl (2.07-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@iit.edu>  Thu,  8 Jun 2006 14:36:49 -0500

libtk-gbarr-perl (2.06-2) unstable; urgency=low

  * Fixed FTBFS due to changed mv behaviour. (Closes: #339869)
  * Upgraded Standards-Version to 3.6.2.
  * Moved debhelper from Build-Depends-Indep to Build-Depends as per policy.

 -- Niko Tyni <ntyni@iki.fi>  Sat, 17 Dec 2005 14:55:49 +0200

libtk-gbarr-perl (2.06-1) unstable; urgency=low

  * Initial Release (Closes: #304287)
  * Maintainer -
    Debian Perl Group <pkg-perl-maintainer@lists.alioth.debian.org>
    via Carlo Segre <segre@iit.edu>

 -- Carlo Segre <segre@iit.edu>  Mon, 14 Mar 2005 00:14:56 -0600
